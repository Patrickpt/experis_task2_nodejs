# Task 2 - NodeJS

This task contains two small projects written in JavaScript with NodeJS:

## ConsoleApp

Simple console app for either reading and logging package.json, displaying hardware information, or starting a HTTP server.

## WebScraper

This application retrieves jokes from the subreddit r/oneliners. The joke and the author of the joke are logged and written to jokes.json.
