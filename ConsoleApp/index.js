const readline = require('readline');
const fs = require('fs');
const os = require('os');
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

console.log(
  `Choose an option:
1. Read package.json
2. Display OS info
3. Start HTTP server`
);

const readJson = () => {
  console.log('Reading package.json...');
  fs.readFile('package.json', (err, data) => {
    if (err) {
      console.log(`Error: ${err}`);
      throw err;
    }
    let res = JSON.parse(data);
    console.log(res);
  });
};

const getOSInfo = () => {
  console.log(`Getting OS info...`);
  console.log(`System memory: ${bytesToGb(os.totalmem())}`);
  console.log(`Free memory: ${bytesToGb(os.freemem())}`);
  console.log(`CPU CORES: ${os.cpus().length}`);
  console.log(`ARCH: ${os.arch()}`);
  console.log(`PLATFORM: ${os.platform()}`);
  console.log(`RELEASE: ${os.release()}`);
  console.log(`USER: ${os.userInfo().username}`);
};

const startHttpServer = () => {
  const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
  });

  server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  });
};

const bytesToGb = (bytes) => {
  //One kb = 1024 byte
  let kb = 1024;

  let gb = kb * kb * kb;
  let decimal = 3;
  return (bytes / gb).toFixed(decimal) + ' GB';
};

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question('Type a number: ', (number) => {
  switch (parseInt(number)) {
    case 1:
      readJson();
      break;
    case 2:
      getOSInfo();
      break;
    case 3:
      startHttpServer();
      break;
    default:
      console.log('Invalid option, please choose a number between 1 and 3');
      break;
  }
  rl.close();
});
