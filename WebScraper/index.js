const cheerio = require('cheerio');
const request = require('request');
const fs = require('fs');

const url = 'https://old.reddit.com/r/oneliners/';
const file = 'jokes.json';
let jokes = [];

console.log('Retrieving jokes...');
request(url, (err, res, body) => {
  if (!err && res.statusCode === 200) {
    let $ = cheerio.load(body);
    let posts = $('a.title'),
      authors = $('a.author');

    posts.each((i) => {
      let joke = posts[i].children[0].data;
      let author = authors[i].children[0].data;
      logJokes(joke, author);
      jokes.push({ joke: joke, author: author });
    });

    let jokesString = JSON.stringify(jokes);

    fs.writeFile(file, jokesString, (err) => {
      if (err) {
        throw err;
      }
      console.log(`Saved jokes to ${file}`);
    });
  }
});

const logJokes = (joke, author) => {
  console.log('\n');
  console.log(joke);
  console.log(`Author: ${author}`);
};
